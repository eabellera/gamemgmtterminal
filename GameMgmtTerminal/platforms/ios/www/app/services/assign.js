﻿(function () {
    'use strict';

    angular.module('app')

    .factory('Assign', Assign);

    Assign.$inject = ['BaseUrls', '$http', 'IonicAlertSvc', '$q'];

    function Assign(BaseUrls, $http, IonicAlertSvc, $q) {
        var service = {
            detailSheetGenerated:  { value: false },
            //getAssignment: getAssignment,
            getDetailSheet: getDetailSheet,
            selectedEmployee: null,
            setSelectedEmployee: setSelectedEmployee
        };

        return service;

            
        //function getAssignment(id) {
        //    return $http.get(BaseUrls.webServer + 'assigns/' + id)
        //        .then(function (result) {
        //            return result.data;
        //        });
        //}

        function getDetailSheet(eventId) {
            return $http.get(BaseUrls.webServer + 'detailsheets/generate?eventId=' + eventId)
                .then(function (result) {
                    return result.data;
                })
                .catch(function (error) {
                    var msg = 'Error generating detail sheet.';
                    if (error.data && error.data.message) {
                        msg = msg + '\n' + error.data.message;
                        error.data.message = msg;
                    }
                    IonicAlertSvc.error(msg, error);
                    return $q.reject(error)
                });
        }

        function setSelectedEmployee(emp) {
            service.selectedEmployee = emp;
        }

         
    }

})();
