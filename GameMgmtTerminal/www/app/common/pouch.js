﻿(function () {
    'use strict';

    angular.module('app')

    .factory('Pouch', Pouch);

    Pouch.$inject = ['BaseUrls', 'IonicAlertSvc', '$q'];

    function Pouch(BaseUrls, IonicAlertSvc, $q) {
        var detailSheetDb = new PouchDB('detail_sheet');
        var remoteDb = new PouchDB(BaseUrls.couchDb);
        var selectedEventDb = new PouchDB('selected_event');
        var positionsDb = new PouchDB('positions');
        var defaultTitlesDb = new PouchDB('default_titles');
        var eventsDb = new PouchDB('events');
        var titlesDb = new PouchDB('titles');

        var service = {
            bulkDefaultTitles: bulkDefaultTitles,
            bulkEvents: bulkEvents,
            bulkPositions: bulkPositions,
            bulkTitles: bulkTitles,
            deleteDetailSheetDb: deleteDetailSheetDb,
            getAllDetailSheet: getAllDetailSheet,
            getAllEvents: getAllEvents,
            getAllPositions: getAllPositions,
            getAllTitles: getAllTitles,
            getSelectedEvent: getSelectedEvent,
            queryByEventId: queryByEventId,
            queryDefaultTitlesbyStaffId: queryDefaultTitlesbyStaffId,
            syncDetailSheet: syncDetailSheet,
            updateDetailSheet: updateDetailSheet,
            updateEvent: updateEvent
        };

        init();

        return service;

        function init() {
            createSelectedEventDoc();
            createQueryDefaultTitlesByStaffId();
            //createQueryDetailSheetByEventId();
        }

        function bulkDefaultTitles(data) {
            return defaultTitlesDb.destroy()
                .then(function () {
                    defaultTitlesDb = new PouchDB('default_titles');
                })
                .then(createQueryDefaultTitlesByStaffId)
                .then(function () {
                    return $q.when(
                        defaultTitlesDb.bulkDocs(data)
                    );
                })
                .then(function () {
                    return $q.when(defaultTitlesDb.query('query/staff_id', {
                        limit: 0
                    })
                    );
                })
                .catch(logError('Update Default Titles Error. '));         
        }

        function bulkEvents(data) {
            return eventsDb.destroy()
                .then(function () {
                    eventsDb = new PouchDB('events');

                    return $q.when(
                        eventsDb.bulkDocs(data)
                    );
                })
                .catch(logError('Update Events Error. '));
        }

        function bulkPositions(data) {
            return positionsDb.destroy()
                .then(function () {
                    positionsDb = new PouchDB('positions');

                    return $q.when(
                        positionsDb.bulkDocs(data)
                    );
                })
                .catch(logError('Update Positions Error. '));
        }

        function bulkTitles(data) {
            return titlesDb.destroy()
                .then(function () {
                    titlesDb = new PouchDB('titles');

                    return $q.when(
                        titlesDb.bulkDocs(data)
                    );
                })
                .catch(logError('Update Titles Error. '));
        }

        function createSelectedEventDoc() {
            // Create default 'selectedEvent' doc. If already exists, nothing will happen.
            selectedEventDb.put({ _id: 'selectedEvent', value: null })
                .catch(function (err) {
                    logNonConflictError(err, 'Error creating \'Selected Event\' doc. ');
                });
        }


        function createQueryDetailSheetByEventId() {
            // Create design doc for query Detail Sheet by Event ID
            return detailSheetDb.put({
                _id: "_design/query",
                views: {
                    eventid: {
                        map: "function(doc){emit(doc.event_ID)}"
                    }
                }
            }).catch(function (err) {
                logNonConflictError(err, 'Error creating \'Detail Sheet by Event ID\' query. ');
            });
        }

        function createQueryDefaultTitlesByStaffId() {
            // Create design doc for query Detail Sheet by Event ID
            return $q.when(defaultTitlesDb.put({
                "_id": "_design/query",
                "views": {
                    "staff_id": {
                        "map": "function(doc){emit(doc.staff_ID.toLowerCase())}"
                    }
                }
            }).catch(function (err) {
                logNonConflictError(err, 'Error creating \'Default Titles by Staff ID\' query. ');
            }));
        }

        function deleteDetailSheetDb(d) {
            return $q.when(
                detailSheetDb.destroy()
                    .then(function () {
                        return detailSheetDb = new PouchDB('detail_sheet'); // recreate (now empty) DB for future detail sheets
                    })
                 //   .then(createQueryDetailSheetByEventId)
                    .catch(logError('Delete detailSheetDb Error. '))
            );
        }

        function getAllDetailSheet() {
            return $q.when(
                detailSheetDb.allDocs({ include_docs: true })
                    .then(function (response) {
                        return response.rows;
                    })
                    .catch(logError('GetAll Detail Sheet Error. '))
            );
        }

        function getAllEvents() {
            return $q.when(
                eventsDb.allDocs({ include_docs: true })
                    .then(function (response) {
                        return response.rows;
                    })
                    .catch(logError('Get All Events Error. '))
            );
        }

        function getAllPositions() {
            return $q.when(
                positionsDb.allDocs({ include_docs: true })
                    .then(function (response) {
                        return response.rows;
                    })
                    .catch(logError('Get All Positions Error. '))
            );
        }

        function getAllTitles() {
            return $q.when(
                titlesDb.allDocs({ include_docs: true })
                    .then(function (response) {
                        return response.rows;
                    })
                    .catch(logError('Get All Titles Error. '))
            );
        }

        function getSelectedEvent() {
            return $q.when(
                selectedEventDb.get('selectedEvent')
                .then(function (response) {
                    return response;
                })
                .catch(logError('Get Selected Event Error. '))
            )
        }

        function logError(message) {
            return function (err) {
                IonicAlertSvc.error(message, err);
            }
        }

        function logNonConflictError(err, msg) {
            if (err.status != 409) {
                logError(msg)(err);
            }
        }

        function queryByEventId(eventId) {
            return $q.when(
                detailSheetDb
                    .query('query/eventid', {
                        key: eventId,
                        include_docs: true
                    })
                    .then(function (response) {
                        return response.rows;
                    })
                    .catch(logError('Query By Event ID Error. '))
            );

            function mapFunction(doc) {
                emit(doc.event_ID);
            }
        }

        function queryDefaultTitlesbyStaffId(staffId) {
            return $q.when(
                defaultTitlesDb
                    .query('query/staff_id', {
                        key: staffId.toLowerCase(),
                        include_docs: true
                    })
                    .then(function (response) {
                        return response.rows;
                    })
                    .catch(logError('Query by Staff ID Error. '))
            );

            function mapFunction(doc) {
                emit(doc.staff_ID);
            }
        }

        function syncDetailSheet() {
            var defer = $q.defer();

            detailSheetDb.sync(remoteDb)
                .on('complete', function () {
                    defer.resolve();
                })
                .on('error', function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

        function updateDetailSheet(doc) {
            return $q.when(detailSheetDb.put(doc)
                .then(function (result) {
                    console.log('update', result);
                })
                .catch(logError('Update Detail Sheet Error. '))
            );
        }

        function updateEvent(doc) {
            return $q.when(selectedEventDb.put(doc)
                .then(function (result) {
                    console.log('update', result);
                })
                .catch(logError('Update Selected Event Error. '))
            );
        }




    }

})();
