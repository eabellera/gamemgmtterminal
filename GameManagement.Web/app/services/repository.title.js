﻿(function () {
    'use strict';

    var serviceId = 'repository.title';
    angular.module('app').factory(serviceId,
        ['model', 'repository.abstract', RepositoryTitle]);

    function RepositoryTitle(model, AbstractRepository) {
        var entityName = model.entityNames.title;
        var EntityQuery = breeze.EntityQuery;
        var orderBy = 'id';
        var Predicate = breeze.Predicate;

        function Ctor(mgr) {
            this.serviceId = serviceId;
            this.entityName = entityName;
            this.manager = mgr;
            // Exposed data access functions
            this.getAllLocal = getAllLocal;
            this.getAll = getAll;
        }

        AbstractRepository.extend(Ctor);

        return Ctor;

        // Formerly known as datacontext.getLocal()
        function getAllLocal() {
            var self = this;
           // var predicate = Predicate.create('isSpeaker', '==', true);
            return self._getAllLocal(entityName, orderBy);
        }

        // Formerly known as datacontext.getSpeakerPartials()
        function getAll(forceRemote, id) {
            var self = this;
            var titles = [];

            var predicate = Predicate
                .create('id', '==', id);

            if (!forceRemote && self._areItemsLoaded()) {
                titles = self._getAllLocal(entityName, orderBy, predicate);
                return self.$q.when(titles);
            }

            return EntityQuery.from('Title')
                .orderBy(orderBy)
                .toType(entityName)
                .using(self.manager).execute()
                .then(querySucceeded, self._queryFailed);

            function querySucceeded(data) {
            	self._areItemsLoaded(true);
                titles = data.results;
                self.log('Retrieved [Titles] from remote data source', titles.length, false);
                return titles;
            }
        }


    }
})();