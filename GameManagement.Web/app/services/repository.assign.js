﻿(function () {
    'use strict';

    var serviceId = 'repository.assign';
    angular.module('app').factory(serviceId,
        ['$http', 'model', 'repository.abstract', RepositoryAssign]);

    function RepositoryAssign($http, model, AbstractRepository) {
        var entityName = model.entityNames.assign;
        var EntityQuery = breeze.EntityQuery;
        var orderBy = 'staff_ID';
        var Predicate = breeze.Predicate;

        function Ctor(mgr) {
            this.serviceId = serviceId;
            this.entityName = entityName;
            this.manager = mgr;
            // Exposed data access functions
            this.create = create;
            this.getAllLocal = getAllLocal;
            this.getAll = getAll;
            this.getByStaffId = getByStaffId;
            this.getReassignments = getReassignments;
            this.getDetailSheet = getDetailSheet;
            this.syncCouchToSql = syncCouchToSql;
        }

        AbstractRepository.extend(Ctor);

        return Ctor;


        function create(){
            return this.manager.createEntity(entityName);
        }

        // Formerly known as datacontext.getLocal()
        function getAllLocal() {
            var self = this;
            // var predicate = Predicate.create('isSpeaker', '==', true);
            return self._getAllLocal(entityName, orderBy);
        }

        // Formerly known as datacontext.getSpeakerPartials()
        function getAll(forceRemote, eventId) {
            var self = this;
            var assigns = [];

            var predicate = Predicate
                .create('event_ID', '==', eventId);

            if (!forceRemote && self._areItemsLoaded()) {
                assigns = self._getAllLocal(entityName, orderBy, predicate);
                return self.$q.when(assigns);
            }

            return EntityQuery.from('Assign')
                .orderBy(orderBy)
                .where(predicate)
                .toType(entityName)
                .using(self.manager).execute()
                .then(querySucceeded, self._queryFailed);

            function querySucceeded(data) {
                self._areItemsLoaded(true);
                assigns = data.results;
                self.log('Retrieved [assigns] from remote data source', assigns.length, false);
                return assigns;
            }
        }

        function getByStaffId(staffId, num) {
            var self = this;
            var assigns = [];
            var take = num || 10;

            var predicate = Predicate
                .create('staff_ID', '==', staffId);

            return EntityQuery.from('Assign')
                .orderByDesc('timeIn')
                .take(take)
                .where(predicate)
                .toType(entityName)
                .using(self.manager).execute()
                .then(querySucceeded, self._queryFailed);

            function querySucceeded(data) {
                assigns = data.results;
                self.log('Retrieved [assigns] from remote data source', assigns.length, false);
                return assigns;
            }
        }

        function getDetailSheet(eventId) {
            var opts = {
                method: 'GET',
                url: 'api/detailsheets?eventId=' + eventId
            };

            return $http(opts)
                .then(onSuccess)
                .catch(self._queryFailed);

            function onSuccess(result) {
                return result.data;
            }
        }

        function getReassignments(eventId) {
            var opts = {
                method: 'GET',
                url: 'api/detailsheets/reassignments?eventId=' + eventId
            };

            return $http(opts)
                .then(onSuccess)
                .catch(self._queryFailed);

            function onSuccess(result) {
                return result.data;
            }
        }

        function syncCouchToSql(eventId) {
            var opts = {
                method: 'GET',
                url: 'api/detailsheets/couch?eventId=' + eventId
            };

            return $http(opts)
                .then(onSuccess)
                .catch(self._queryFailed);

            function onSuccess(result) {
                return result.data;
            }
        }

    }
})();