﻿(function () {
    'use strict';

    angular.module('common').factory('calculators', [calculators]);

    function calculators() {

        var service = {
            calculateAssignmentTotals: calculateAssignmentTotals
        };

        return service;


        function calculateAssignmentTotals(assignedStaff) {
            var positions = [];

            var eventTotals = {};
            eventTotals.positions = [];
            eventTotals.gross = 0;
            eventTotals.hours = 0;

            for (var i = 0; i < assignedStaff.length; i++) {
                eventTotals.gross += assignedStaff[i].gross;
                eventTotals.hours += assignedStaff[i].hours;

                var positionIndex;
                var found = eventTotals.positions.some(function (current, index) {
                    positionIndex = index;
                    return current.name == assignedStaff[i].position;
                });

                if (found) {
                    eventTotals.positions[positionIndex].count++;
                }
                else {
                    var positionObj = {};
                    positionObj.name = assignedStaff[i].position;
                    positionObj.count = 1;
                    eventTotals.positions.push(positionObj);
                }
            }

            return eventTotals;
        }




    }
})();