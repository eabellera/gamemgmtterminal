﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using GameManagement.Models;
using GameManagement.Models.ViewModels;

namespace GameManagement.Controllers
{
    public class EventsController : ApiController
    {
        private GameMangementEntities db = new GameMangementEntities();


        [Route("api/convertassigns"), HttpGet]
        public int ConvertAssigns()
        {
            var assigns = db.Assigns;

            try
            {
                DateTime combinedIn;
                DateTime combinedOut;
                DateTime eventDate;

                foreach (var item in assigns)
                {
                    if (item.EventDate.HasValue)
                    {
                        eventDate = item.EventDate.Value;

                        if (item.Report_Time.HasValue)
                        {
                            combinedIn = eventDate.Add(item.Report_Time.Value.TimeOfDay);
                            item.TimeIn = TimeZone.CurrentTimeZone.ToUniversalTime((DateTime)combinedIn);
                        }

                        if (item.OutTime.HasValue)
                        {
                            combinedOut = eventDate.Add(item.OutTime.Value.TimeOfDay);
                            item.TimeOut = TimeZone.CurrentTimeZone.ToUniversalTime((DateTime)combinedOut);
                        }
                    }

                }


                return db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        [Route("api/convert"), HttpGet]
        public int Convert()
        {
            var events = db.Events;

            try
            {
                DateTime timeOnly;

                foreach (var item in events)
                {
                    if (item.Event_Time.HasValue)
                    {
                        //item.TimeUtc = item.Event_Time;

                        timeOnly = TimeZone.CurrentTimeZone.ToUniversalTime((DateTime)item.TimeUtc);

                        if (item.Event_Date.HasValue)
                        {
                            item.TimeUtc = item.Event_Date.Value.Add(timeOnly.TimeOfDay);
                        }

                    }
                    else
                    {
                        item.TimeUtc = item.Event_Date;
                    }
                }


                return db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        // GET: api/Events
        public IQueryable<object> GetEvents()
        {
            var query = db.Events.Select(x =>
               new EventViewModel
               {
                   Id = x.Id,
                   Event_name = x.Event_name,
                   Event_Date = x.Event_Date,
                   Event_Time = x.Event_Time,
                   Event_Sport = x.Event_Sport,
                   Event_Venue = x.Event_Venue,
                   TV = x.TV,
                   TimeUtc = x.TimeUtc,
                   PayPeriodId = x.PayPeriodId

               });

            return query;
        }

        // GET: api/Events/5
        [ResponseType(typeof(Event))]
        public IHttpActionResult GetEvent(string id)
        {
            Event @event = db.Events.Find(id);
            if (@event == null)
            {
                return NotFound();
            }

            return Ok(@event);
        }

        // PUT: api/Events/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEvent(string id, Event @event)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != @event.Id)
            {
                return BadRequest();
            }

            db.Entry(@event).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Events
        [ResponseType(typeof(Event))]
        public IHttpActionResult PostEvent(Event @event)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Events.Add(@event);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (EventExists(@event.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = @event.Id }, @event);
        }

        // DELETE: api/Events/5
        [ResponseType(typeof(Event))]
        public IHttpActionResult DeleteEvent(string id)
        {
            Event @event = db.Events.Find(id);
            if (@event == null)
            {
                return NotFound();
            }

            db.Events.Remove(@event);
            db.SaveChanges();

            return Ok(@event);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EventExists(string id)
        {
            return db.Events.Count(e => e.Id == id) > 0;
        }
    }
}