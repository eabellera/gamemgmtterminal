//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GameManagement.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PayRate
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PayRate()
        {
            this.Titles = new HashSet<Title>();
        }
    
        public int Id { get; set; }
        public string PayCode { get; set; }
        public Nullable<decimal> Regular { get; set; }
        public Nullable<decimal> OTS_B_DISP { get; set; }
        public Nullable<decimal> OTS_M_DISP { get; set; }
        public Nullable<decimal> OTP_B_DISP { get; set; }
        public Nullable<decimal> OTP_M_DISP { get; set; }
        public Nullable<decimal> RegCalc { get; set; }
        public Nullable<decimal> OTCalc { get; set; }
        public Nullable<decimal> Timesheet { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Title> Titles { get; set; }
    }
}
