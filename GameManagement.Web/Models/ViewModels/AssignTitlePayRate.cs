﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameManagement.Models.ViewModels
{
    public class AssignTitlePayRate
    {
        public Assign Assign { get; set; }
        public Title Title { get; set; }
        public PayRate PayRate { get; set; }
    }
}