﻿using GameManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameManagement
{
    public class ConvertToUtc
    {
        private GameMangementEntities db = new GameMangementEntities();


        public int Convert()
        {
            var events = db.Events;

            try
            {
                foreach (var item in events)
                {
                    if (item.Event_Time.HasValue)
                    {
                        item.TimeUtc  = TimeZone.CurrentTimeZone.ToUniversalTime((DateTime)item.Event_Time);
                    }
                }


                return db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}